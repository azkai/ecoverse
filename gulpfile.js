// Set all Variables
// ------------------------------------------------------------------------
var   gulp         = require('gulp')
    , watch        = require('gulp-watch')
    , sass         = require('gulp-sass')
    , server       = require('gulp-express')
    , sync         = require('browser-sync')
    , gutil        = require('gulp-util')
    , sourcemaps   = require('gulp-sourcemaps')
    , notify       = require('gulp-notify')
    , plumber      = require('gulp-plumber')
    , uglify       = require('gulp-uglify')
    , changed      = require('gulp-changed')
    , autoprefixer = require('gulp-autoprefixer')
    , connect      = require('gulp-connect-php')
    , reload       = sync.reload
    ;

// Variables for Servers and Files
// -------------------------------

var dir = ({
      view        : 'views/'
    , views       : {
        css       : 'css/'
      , js        : 'js/'
      }
    , models      : 'models/'
    , controllers : 'controllers/'
    , compile     : '_sources/'
    , sources     : {
        scss      : 'scss/'
      , js        : 'js/'
      , templates : 'templates/'
    }
});
// End all Variables / Objects

// Static Server
// ------------------------------------------------------------------------
gulp.task('serve', ['sass', 'js'], function() {

    server.run({
        file: 'app.js'
    });

    sync({
        open: false ,
        proxy: "localhost:9999"
    }); // End Sync

    // Watches Files while server is running
    // -------------------------------------
    gulp.watch(dir.view + dir.compile + dir.sources.scss + "**/*.scss", ['sass']);
    gulp.watch(dir.view + dir.compile + dir.sources.js + "**/*.js", ['js']);
    gulp.watch(dir.view + "**/*.html").on('change', reload);
    gulp.watch(dir.view + "**/*.php").on('change', reload);

}); // End Server

// Compile sass into CSS & auto-inject into browsers
// ------------------------------------------------------------------------
gulp.task('sass', function() {
    return gulp.src(dir.view + dir.compile + dir.sources.scss + "stylesheet.scss")  //<-- Give gulp a single file to start with
        .pipe(sourcemaps.init())
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sass({
            errLogToConsole: true,
            onError: function(err) {
                return notify().write(err);
            }
        }))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
            onError: function(err) {
                return notify().write(err);
            }
        }))
        .pipe(gulp.dest(dir.view + dir.views.css))
        .on('error', gutil.log)
        .pipe(reload({stream: true}));
}); // End SASS Task


// Compress and Move JS to Prod
// ------------------------------------------------------------------------
gulp.task('js', function() {
  return gulp.src(dir.view + dir.compile + dir.sources.js + "**/*.js")
    .pipe(changed(dir.view + dir.views.js))
    .pipe(uglify())
    .pipe(gulp.dest(dir.view + dir.views.js))
    .pipe(reload({stream: true}));
});


// ------------------------------------------------------------------------
// Final Task to Run
// ------------------------------------------------------------------------
   gulp.task('dev', ['serve']);
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------