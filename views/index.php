<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <title>(2) Roleplay Online</title>
    <meta charset="utf-8">
    <meta name="description" content="Roleplay online on the RPGuide forums or find Roleplay partners for any online MMORP game, including: World of Warcraft, Final Fantasy, Aion, Dungeons and Dragons, Furcadia, Eve Online, Guild Wars and many more! Roleplay online for free without any MMORPG too on EcoVerse.com!">
  </head>
  <body>
  <!-- GLOBAL NAV: START -->
    <header class="nav" role="navigation" data-grid="row" data-span="desktop-13">
      <nav class="menu-global" data-grid="col" data-span="desktop-13">
        <ul class="menu-list">
          <li><strong>RPG</strong></li>
          <li><a href="">Activity</a></li>
          <li><a href="">Find</a></li>
          <li><a href="">Socialize</a></li>
          <li><a href="">Help</a></li>
        </ul>
      </nav>
      <div class="menu-user-box" data-grid="col" data-span="desktop-13">
        <ul class="menu-user">
          <li id="my-account" class="my-account"><span>Welcome Home <a href="">Kai</a>!</span></li>
        </ul>
      </div>
    </header>

    <!-- SIDE MENU: START -->
    <aside class="menu-side" data-grid="row" data-span="desktop-2">
      <ul class="nav-secondary">
        <li><a href="" aria-label="Characters">Characters</a></li>
        <li><a href="" aria-label="Messages">Messages <span data-type="notify">12</span></a></li>
        <li><a href="" aria-label="Updates">Updates</a></li>
        <li><a href="" aria-label="Account Settings">My Account</a></li>
      </ul>
    </aside>

    <!-- MAIN CONTENT: START -->
    <!-- May want to start view content here? -->
    <main data-grid="row" data-span="desktop-11">

    <!-- PLAYER PROFILE: START -->

      <!-- CHARACTER COVER - BASICS: START -->
      <section class="character-cover" data-grid="row" data-span="desktop-11">
        <div class="character-image" data-grid="col" data-span="desktop-11">
          <img src="/images/natasha-cover.png" alt="">
        </div>
        <div class="character-info" data-grid="col" data-span="desktop-9">
          <div class="character-portrait" data-grid="col" data-span="desktop-1"><img src="http://www.rprepository.com/images/gallery/gl-21680-1417359863.png" alt=""></div>
          <div class="character-basics" data-grid="col" data-span="desktop-5">
            <ul>
              <li><h1>Natasha Vaughn <span id="character-nickname">(Kitty)</span></h1></li>
              <li class="sub">Created by <a href="" title="" data-type="user">Elizabeth</a></li>
              <li>Female Long-Haired Calico, 21</li>
              <li>Vintner Socialite</li>
            </ul><!-- end basic information -->
          </div>
        </div><!-- end character info -->
        <ul class="character-games">
          <li><a href="" aria-label="EcoVerse"></a></li>
          <li><a href="" aria-label="Furcadia"></a></li>
        </ul>
      </section>
      <!-- END: character cover and basics -->

      <section class="character-profile">
        <div class="character-nav">
          <ul class="menu-character-profile" data-grid="col" data-span="desktop-11">
            <li><a href="" class="active">Basics</a></li>
            <li><a href="">Stories</a></li>
            <li><a href="">Art</a></li>
            <li><a href="">Relations</a></li>
          </ul>
        </div>

        <section class="character-stats">

        <!-- BASIC STATS: Start -->
          <div>
            <h3>Basic Stats</h3>
            <table>
              <colgroup span="1" class="character-stats-col"></colgroup>
              <thead>
                <tr>
                  <th aria-label="Stat"></th>
                  <th aria-label="Value"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Age</td>
                  <td>21</td>
                </tr>
                <tr>
                  <td>Birthday</td>
                  <td>Aug 2 <span class="sub zodiac-leo" title="Zodiac is Leo" aria-label="Zodiac is Leo"></span></td>
                </tr>
                <tr>
                  <td>Sex</td>
                  <td>Female</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td><abbr title="A person whose personality type identifies with their sex.">Cisgender</abbr></td>
                </tr>
                <tr>
                  <td>Species</td>
                  <td>Feline</td>
                </tr>
                <tr>
                  <td>Race</td>
                  <td>Long Haired Calico</td>
                </tr>
                <tr>
                  <td>Core Class</td>
                  <td>Aristocrat</td>
                </tr>
                <tr>
                  <td>Variant Class</td>
                  <td>Socialite</td>
                </tr>
                <tr>
                  <td>Occupation</td>
                  <td>Vintner</td>
                </tr>
                <tr>
                  <td>Height</td>
                  <td>5'8"</td>
                </tr>
                <tr>
                  <td>Body Shape</td>
                  <td>Pear</td>
                </tr>
                <tr>
                  <td>Eyes</td>
                  <td>Almond shaped, verdant green</td>
                </tr>
                <tr>
                  <td>Hair</td>
                  <td>Long flowing hair as dark as coal with flaming orange tips at their end</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- END: Basic Stats -->

          <!-- RELATIONSHIPS: Start -->
          <div>
            <h3>Relationships</h3>
            <table>
              <colgroup span="1" class="character-stats-col"></colgroup>
              <thead>
                <tr>
                  <th aria-label="Stat"></th>
                  <th aria-label="Value"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Status</td>
                  <td>In a Relationship</td>
                </tr>
                <tr>
                  <td>Sexuality</td>
                  <td><abbr title="A person who is attracted to the opposite sex.">Heterosexual</abbr></td>
                </tr>
                <tr>
                  <td>Role</td>
                  <td>Mistress</td>
                </tr>
                <tr>
                  <td>Position</td>
                  <td>Switch</td>
                </tr>
                <tr>
                  <td>Master</td>
                  <td>AzKai</td>
                </tr>
                <tr>
                  <td>Submissives</td>
                  <td>Caramelle, Juliet</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- END: Relationships -->

          <!-- ABILITY SCORES: Start -->
          <div>
            <h3>Ability Scores</h3>
            <table class="character-abilities">
              <colgroup span="1" class="character-stats-col"></colgroup>
              <thead>
                <tr>
                  <th aria-label="Stat"></th>
                  <th aria-label="Value"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Strength</td>
                  <td>8</td>
                </tr>
                <tr>
                  <td>Dexterity</td>
                  <td>12</td>
                </tr>
                <tr>
                  <td>Constitution</td>
                  <td>10</td>
                </tr>
                <tr>
                  <td>Intelligence</td>
                  <td>14</td>
                </tr>
                <tr>
                  <td>Wisdom</td>
                  <td>13</td>
                </tr>
                <tr>
                  <td>Charisma</td>
                  <td>17</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- END: Ability Scores -->

          <!-- LOOKING FOR: Start -->
          <div>
            <h3>Looking For</h3>
            <span class="label">Work</span>
            <span class="label">Friends</span>
            <span class="label">Antagonists</span>
          </div>
          <!-- END: Looking For -->

        </section>
        <!-- END: Basic Stat Columns -->


        <!-- LONG DESCRIPTIONS: Start -->
        <section class="character-descriptions">
          <div>
            <h3>Physical Description</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div>
            <h3>Personality</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div>
            <h3>Background</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>   
        </section>
        <!-- END: Long Descriptions -->
      </section>
      <!-- END: Character Profile DIV -->
    </main>

    <!-- <footer role="contentinfo">
    </footer> -->

    <!-- PAGE SCRIPTS: Start -->
    <script src="/js/jquery-2.1.4.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/plugins.js"></script>
    <!-- END: Page Scripts -->
  </body>
</html>