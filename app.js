// use PHP as view engine in Express
var express = require('express'),
    app = express(),
    phpnode = require('php-node')({bin:"\"C:\\Program Files (x86)\\Ampps\\php-5.4\\php.exe\""});

var render = require('php-node')({bin:"\"C://Program Files (x86)//Ampps//php-5.4//php.exe\""});

render(__dirname+'\\views\\index.php', {}, function(e, r) {
})

app.set('views', __dirname+'/views');
app.engine('php', phpnode);
app.set('view engine', 'php');
app.use(express.static(__dirname+'/views'));

app.get('/', function(req, res){
  res.render('index.php');
});

var port = process.env.PORT || 9999;
app.listen(port, function() {
  console.log("Listening on " + port);
})